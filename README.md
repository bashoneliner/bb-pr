#Create Pull Requests in Bitbucket.org via the commandline.

### Usage:
|  Opt  |     Long Version        |                            Description                        |
|-------|-------------------------|---------------------------------------------------------------| 
|  -D   |  --description          | PR Description                                                |
|  -d   |  --destination          | Destination branch                                            |
|  -s   |  --source               | Source branch                                                 |
|  -t   |  --title                | PR title                                                      |
|  -u   |  --user                 | BB username                                                   |
|  -p   |  --password             | BB password                                                   |
|  -r   |  --reviewers            | Comma delimited list of reviewer usernames                    |
|  -c   |  --close-source-branch  | Close the source branch on merge (Takes no arguments)         |
|  -o   |  --print-full-output    | Print the full output returned by the API (Takes no arguments)|

### Destination, source, title, user, and password will be prompted for interactively if not provided.
### Without -o option a link for the PR will be returned on success or the json of the error on failure.
### Username and password can also be grabbed from the environment by setting BB_USERNAME and BB_PASSWORD

Works with API 2.0
